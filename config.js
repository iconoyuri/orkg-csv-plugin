let apiEndpoint = 'https://orkg.org'
let sheet = SpreadsheetApp.getActiveSheet()

let requiredColumns = ['paper:title', 'paper:doi']
let customProperties = [
	'paper:authors',
	'paper:publication_month',
	'paper:publication_year',
	'paper:research_field',
	'paper:doi',
	'paper:url',
	'paper:published_in',
	'contribution:research_problem'
]

let firstRow = sheet.getRange(1, 1, 1, sheet.getLastColumn() || 1)

let errorLogs = []

const defaultColor = (range) => range.setBackgroundRGB(255, 255, 255) // white
const dangerColor = (range) => range.setBackgroundRGB(255, 100, 100) // red
const correctColor = (range) => range.setBackgroundRGB(118, 255, 113) // green
const modificationColor = (range) => range.setBackgroundRGB(97, 134, 255) // blue
const warningColor = (range) => range.setBackgroundRGB(255, 246, 113) // yellow

const colorRange = (range, colorationMethod) => {
	colorationMethod(range)
}

function onOpen() {
	SpreadsheetApp.getUi()
		.createMenu('ORKG')
		.addItem('Sidebar', 'displayMain')
		.addToUi()
	let table = sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns())
	table.setFontFamily('Poppins')
	table.setFontSize(8)
	table.setFontColor('#4d4d4d')
}

function insertProperty(property) {
	let values = firstRow.getValues()[0]
	values = values.map((value) => value.trim())
	if (values.indexOf(property) == -1) {
		// Verifying if the property isn't yet inserted in the sheet
		for (let i = 1; i <= sheet.getMaxColumns(); i++) {
			try {
				if (
					sheet
						.getRange(
							1,
							i,
							sheet.getLastRow() || sheet.getMaxRows()
						)
						.isBlank()
				) {
					let headingCell = sheet.getRange(1, i)
					headingCell.setValue(property)
					break
				}
			} catch (e) {
				Logger.log(e)
			}
		}
	}
}

function onSelectionChange(e) {}

function getCurrentProperty() {
	let activeCell = sheet.getActiveRange()
	let { content: value } = getDataAndType(activeCell.getValue())

	if (activeCell.getRow() == 1 && value !== '') {
		if (
			requiredColumns.indexOf(value) != -1 ||
			customProperties.indexOf(value) != -1
		)
			return {
				label: value,
				id: value
			}
		try {
			let response = UrlFetchApp.fetch(
				`${apiEndpoint}/api/predicates/${value}`
			)
			let data = JSON.parse(response.getContentText())
			// colorRange(activeCell, correctColor)
			return {
				label: data?.label || '-',
				id: value
			}
		} catch (e) {
			return {
				label: '-',
				id: value
			}
			// errorLogs.push({message:`not a valid property <${value}>`, range: activeCell.getA1Notation(), status: 'danger'})
			// colorRange(activeCell, dangerColor)
		}
	}
	return {
		label: '-',
		id: '-'
	}
}

function getCurrentResource() {
	let activeCell = sheet.getActiveRange()
	let { content: value } = getDataAndType(activeCell.getValue())
	// let value = activeCell.getValue()
	if (activeCell.getRow() != 1 && value != '') {
		// No resource are at the row 1
		try {
			value = value.toUpperCase()
			let response = UrlFetchApp.fetch(
				`${apiEndpoint}/api/resources/${value}`
			)
			let data = JSON.parse(response.getContentText())

			if (data?.label) activeCell.setValue(value)

			return {
				label: data?.label,
				id: value
			}
		} catch (e) {
			return {
				label: '-',
				id: value
			}
		}
	}
	return {
		label: '-',
		id: '-'
	}
}

function paperTitleOrDOICheckout() {
	if (!paperTitleOrDOIFilled()) {
		colorRange(firstRow, dangerColor)
		errorLogs.push({
			message: 'paper:doi or paper:title not inserted',
			range: firstRow.getA1Notation(),
			status: 'danger'
		})
		return false
	} else {
		colorRange(firstRow, correctColor)
		return true
	}
}
function startValidation() {
	let ok = true
	ok = paperTitleOrDOICheckout()
	if (ok) ok = checkLiteralsDataType()
	if (ok) ok = checkColumnsTyping()
	return errorLogs
}

function isResource(val) {
	if (!typeof val === 'string') return false
	if (val.length < 2) return false
	if (val.charAt(0).toUpperCase() !== 'R') return false
	if (Number(val.substring(1, val.length))) return true
	else return false
}

function cancelValidation() {
	colorRange(
		sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()),
		defaultColor
	)
}

function checkColumnsTyping() {
	for (let i = 1; i <= sheet.getLastColumn(); i++) {
		let column = sheet.getRange(1, i, sheet.getLastRow())
		let columnData = sheet.getRange(2, i, sheet.getLastRow())
		let { dataType: propertyDataType } = getDataAndType(
			sheet.getRange(1, i).getValue()
		)
		let columnDataType = fetchRecommendedDataType(columnData)
		if (propertyDataType !== columnDataType) {
			errorLogs.push({
				message: 'property type mismatch with data',
				range: column.getA1Notation(),
				status: 'warning'
			})
			colorRange(column, warningColor)
		} else {
			colorRange(column, correctColor)
		}
	}
}

function paperTitleOrDOIFilled() {
	let values = firstRow.getValues()[0]
	values = values.map((value) => getDataAndType(value).content)
	for (let i in requiredColumns) {
		if (values.indexOf(requiredColumns[i]) != -1) return true
	}
	return false
}

function paperDOIColumnIndex() {
	let values = firstRow.getValues()[0]
	values = values.map((value) => getDataAndType(value).content)
	let index = values.indexOf('paper:doi')
	if (index == -1) throw new RangeError('not found')
	return index + 1
}

function resolveDOI() {
	let doiColumn = sheet
		.getRange(2, paperDOIColumnIndex(), sheet.getLastRow() - 1)
		.getValues()

	doiColumn = doiColumn.map((row) => getDataAndType(row[0]).content)
	let titles = []
	let authors = []
	let urls = []
	for (let doi of doiColumn) {
		try {
			let response = UrlFetchApp.fetch(
				`https://api.crossref.org/v1/works/${doi}`
			)
			let data = JSON.parse(response.getContentText())

			let title = data.message.title

			let author = data.message.author.reduce(
				(acc, val) => `${acc}${val?.given} ${val?.family}; `,
				''
			)
			author = author.substring(0, author.length - 2)

			let url = data.message.link.reduce(
				(acc, val) => `${acc}${val?.URL}; `,
				''
			)
			url = url.substring(0, url.length - 2)

			titles.push(title)
			authors.push(author)
			urls.push(url)
		} catch (e) {
			titles.push('')
			authors.push('')
			urls.push('')
		}
	}

	insertField('paper:title', titles)
	insertField('paper:authors', authors)
	insertField('paper:url', urls)
}

function insertField(field, data) {
	insertProperty(field)
	let values = firstRow.getValues()[0]
	values = values.map((value) => getDataAndType(value.trim()).content)
	let index = values.indexOf(field) + 1
	if (index == 0) index = sheet.getLastColumn()

	let dataGrid = data.map((value) => [value])

	sheet.getRange(2, index, sheet.getLastRow() - 1).setValues(dataGrid)
}

function getCurrentRange() {
	return sheet.getActiveRange().getA1Notation()
}

function setDataType(dataType) {
	let activeRange = sheet.getActiveRange()
	for (let i = activeRange.getRow(); i <= activeRange.getLastRow(); i++) {
		for (
			let j = activeRange.getColumn();
			j <= activeRange.getLastColumn();
			j++
		) {
			let cell = sheet.getRange(i, j)
			let value = cell.getValue()
			let { content } = getDataAndType(value)
			cell.setValue(`${content}<${dataType}>`)
		}
	}
}

function fetchRecommendedDataType(range) {
	let max = 0
	let recommendedDataType = ''
	let typesMap = {
		text: 0,
		integer: 0,
		decimal: 0,
		boolean: 0,
		url: 0,
		date: 0,
		unknown: 0
	}
	let typesArray = [
		'text',
		'integer',
		'decimal',
		'boolean',
		'url',
		'date',
		'unknown'
	]

	let activeRange = range ?? sheet.getActiveRange()
	for (let i = activeRange.getRow(); i <= activeRange.getLastRow(); i++) {
		for (
			let j = activeRange.getColumn();
			j <= activeRange.getLastColumn();
			j++
		) {
			let cellValue = sheet.getRange(i, j).getValue()
			if (isResource(cellValue)) continue
			let { content } = getDataAndType(cellValue)
			let dataType = getRecommendedType(content)
			typesMap[dataType]++
		}
	}
	for (let type of typesArray) {
		if (typesMap[type] > max) {
			max = typesMap[type]
			recommendedDataType = type
		}
	}
	return recommendedDataType
}

function getRecommendedType(val) {
	let value = val.toString().trim()
	let isText = typeof value === 'string'
	let isDecimal = !!Number(value)
	let isInteger =
		isDecimal && value.indexOf(',') == -1 && value.indexOf('.') == -1
	let isDate = !!(isText && Date.parse(value))
	let isBoolean =
		value.toLowerCase() === 'true' || value.toLowerCase() === 'false'

	let isURL = isValidUrl(value)
	return isDecimal
		? isInteger
			? 'integer'
			: 'decimal'
		: isBoolean
		? 'boolean'
		: isText
		? isDate
			? 'date'
			: isURL
			? 'url'
			: 'text'
		: 'unknown'
}

function isValidUrl(urlString) {
	var urlPattern = new RegExp(
		'^(https?:\\/\\/)?' + // validate protocol
			'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // validate domain name
			'((\\d{1,3}\\.){3}\\d{1,3}))' + // validate OR ip (v4) address
			'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // validate port and path
			'(\\?[;&a-z\\d%_.~+=-]*)?' + // validate query string
			'(\\#[-a-z\\d_]*)?$',
		'i'
	) // validate fragment locator
	return !!urlPattern.test(urlString)
}

function getDataAndType(val) {
	try {
		let value = val.trim()
		let openingChevronIndex = value.lastIndexOf('<')
		let closingChevronIndex = value.lastIndexOf('>')
		let dataType = value.substring(
			openingChevronIndex + 1,
			closingChevronIndex
		)
		let content = value.substring(
			0,
			openingChevronIndex != -1 ? openingChevronIndex : value.length
		)
		return { content, dataType }
	} catch (e) {
		return { content: val, dataType: '' }
	}
}

function checkCell(cell) {
	if (cell.isBlank()) return true
	if (isResource(cell.getValue())) {
		colorRange(cell, modificationColor)
		return true
	}

	let value = cell.getValue()
	if (typeof value === 'string') value = value.trim()
	let { content, dataType } = getDataAndType(value)
	if (dataType) {
		let recommendedType = getRecommendedType(content)
		if (recommendedType !== dataType) {
			colorRange(cell, warningColor)
			errorLogs.push({
				message: `data type <${recommendedType}> is a better fit`,
				range: cell.getA1Notation(),
				status: 'warning'
			})
			return false
		} else {
			colorRange(cell, correctColor)
			return true
		}
	} else {
		errorLogs.push({
			message: `data type not set`,
			range: cell.getA1Notation(),
			status: 'warning'
		})
		colorRange(cell, warningColor)
		return false
	}
}

function checkLiteralsDataType() {
	for (let i = 2; i <= sheet.getLastRow(); i++) {
		for (let j = 1; j <= sheet.getLastColumn(); j++) {
			let cell = sheet.getRange(i, j)
			if (cell.isBlank()) continue
			let accepted = checkCell(cell)
			if (!accepted) return false
		}
	}
	return true
}

function displayMain() {
	const html = HtmlService.createTemplateFromFile('main').evaluate()
	html.setTitle('ORKG')
	SpreadsheetApp.getUi().showSidebar(html)
}
